import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Main from '../Main';
import { Login, Register, TaskPage } from '../pages';
import { useSelector } from 'react-redux';

const Routes = () => {
  const token = useSelector((state) => state.login.token);
  const userId = useSelector((state) => state.login.user.id);

  return (
    <Switch>
      <Route exact path="/">
        <Main />
      </Route>
      {!token ? (
        <>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
        </>
      ) : (
        <Route path={`/tasks/${userId}`}>
          <TaskPage />
        </Route>
      )}
    </Switch>
  );
};

export default Routes;
