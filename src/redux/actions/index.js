import { LOGIN_ACTIONS } from './action-types';
import axios from 'axios';

const login_url = `https://pietroski-task-list-demo.herokuapp.com/login`;
const register_url = `https://pietroski-task-list-demo.herokuapp.com/register`;
const users_url = `https://pietroski-task-list-demo.herokuapp.com/users`;
const tasks_url = `https://pietroski-task-list-demo.herokuapp.com/tasks`;

const login_successful = (token, user, taskList) => ({
  type: LOGIN_ACTIONS.SUCCESSFUL,
  token: token, user: { ...user }, taskList: [ ...taskList ]
});

const login_failed = () => ({
  type: LOGIN_ACTIONS.FAILED
});

export const login_action = (loginData, history) => (dispatch) => {
  axios({
    headers: { "Content-Type": "application/json" },
    method: "post",
    url: login_url,
    data: { ...loginData }
  })
    .then(resp => {
      const token = resp.data.accessToken;
      axios(users_url).
        then(secResp => {
          const user = secResp.data.find(e => e.email === loginData.email);
          const userId = user.id;
          axios(tasks_url)
            .then(tercResp => {
              dispatch(login_successful(token, user, tercResp.data));
              setTimeout(() => history.push(`/tasks/${userId}`), 500);
            })
            .catch(error => dispatch(login_failed()));
      }).catch(error => dispatch(login_failed()));

    })
    .catch(error => {dispatch(login_failed())});
}

export const logout_action = () => ({type: LOGIN_ACTIONS.LOGOUT});

export const register = (registerData, history) => (dispatch) => {
  axios({
    headers: { "Content-Type": "application/json" },
    method: "post",
    url: register_url,
    data: {
      firstName: registerData.firstName,
      lastName: registerData.lastName,
      username: registerData.username,
      email: registerData.email,
      password: registerData.password
    }
  })
    .then(resp => {
      const loginData = {email: registerData.email, password: registerData.password};
      dispatch(login_action(loginData, history));
    })
    .catch(error => {dispatch(login_failed())});
}

export const post_task = (userId, token, task, callback, type = "post", cardId) => (dispatch) => {
  axios({
    headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` },
    method: type,
    url: type === "post" ? tasks_url : `${tasks_url}/${cardId}`,
    data: {
      userId: userId,
      todo: task
    }
  })
    .then(() => {
      get_updated_task_list(dispatch);
      callback(false);
    })
    .catch(error => error);
}

export const get_updated_task_list = (dispatch) => {
  axios(tasks_url)
    .then(resp => dispatch(update_task_list(resp.data)))
    .catch(error => error);
}

const update_task_list = (newTaskList) => ({
  type: LOGIN_ACTIONS.UPDATE, newTaskList
});

export const task_remover = () => {}