import { LOGIN_ACTIONS } from '../../actions/action-types';

const defaultState = {
  token: "",
  user: {},
  taskList: [],
  hadError: undefined
};

const reducer = (state = JSON.parse(localStorage.getItem('pietroski-task-list')) || defaultState, action) => {
  switch (action.type) {
    case LOGIN_ACTIONS.SUCCESSFUL:
      state = {
        ...state,
        token: action.token,
        user: { ...action.user },
        taskList: [ ...action.taskList ],
        hadError: false
      };
      localStorage.setItem('pietroski-task-list', JSON.stringify(state));
      return state;
    
    case LOGIN_ACTIONS.FAILED:
      state = {
        ...state,
        token: "",
        user: {},
        taskList: [],
        hadError: true
      };
      localStorage.setItem('pietroski-task-list', JSON.stringify(state));
      return state;

    case LOGIN_ACTIONS.LOGOUT:
      state = {
        ...state,
        token: "",
        user: {},
        taskList: [],
        hadError: undefined
      };
      localStorage.removeItem('pietroski-task-list');
      return state;

    case LOGIN_ACTIONS.UPDATE:
      state = {
        ...state,
        taskList: action.newTaskList
      }
      localStorage.setItem('pietroski-task-list', JSON.stringify(state));
      return state;

    default:
      return state;
  }
};

export default reducer;
