import React from 'react';
import './App.css';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Main = () => {
  const history = useHistory();
  const isLogged = useSelector((state) => state.login.token);
  const userId = useSelector((state) => state.login.user.id);

  return (
    <div className="App">
      <header className="App-header">
        <h1>Task List - Demo</h1>
        <img
          src={'https://pietroski.gitlab.io/pytroski/Roma_Aeterna.jpg'}
          className="App-logo"
          alt="logo"
        />
        <p>
          Written by&nbsp;
          <a
            href="https://www.linkedin.com/in/augusto-pietroski/"
            rel="noreferrer noopener"
            target="_blank">
            Augusto Pietroski
          </a>
        </p>
        <StyledButton
          onClick={() => {
            isLogged ? history.push(`tasks/${userId}`) : history.push('/login');
          }}>
          Begin
        </StyledButton>
      </header>
    </div>
  );
};

export default Main;

const StyledButton = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 50px;
  background-color: #c4c4c4;
  color: #000;
  border-radius: 10px;
  border: 3px solid black;

  &:hover {
    border: 4px solid black;
    cursor: pointer;
  }
`;
