import React, { useState } from 'react';
import { StyledInputTask, StyledInput, ButtonHolder } from './styled';
import { useWindowSize } from '../../hooks';
import { ImCheckmark, ImCross } from 'react-icons/im';
import { useDispatch, useSelector } from 'react-redux';
import { post_task } from '../../redux/actions';

const InputTask = ({ onHold }) => {
  const [width] = useWindowSize();
  const [task, setTask] = useState('');

  const token = useSelector((state) => state.login.token);
  const userId = useSelector((state) => state.login.user.id);
  const dispatch = useDispatch();

  const handleSetTask = ({ target: { value } }) => {
    setTask(value);
  };

  return (
    <StyledInputTask width={width}>
      <StyledInput value={task} onChange={handleSetTask} />
      <ButtonHolder>
        <ImCross
          color={'darkred'}
          size={24}
          className="btn"
          onClick={() => {
            onHold(false);
          }}
        />
        <ImCheckmark
          color={'darkgreen'}
          size={32}
          className="btn"
          onClick={() => {
            task && dispatch(post_task(userId, token, task, onHold));
          }}
        />
      </ButtonHolder>
    </StyledInputTask>
  );
};

export default InputTask;
