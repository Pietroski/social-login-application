import React from 'react';
import renderer from 'react-test-renderer';

import InputTask from '../';

describe('Snapshot Tests', () => {
  it('renders page/component', () => {
    const tree = renderer.create().toJSON();
    expect(tree).toMatchSnapshot();
  });
});
