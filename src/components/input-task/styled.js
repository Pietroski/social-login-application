import styled from 'styled-components';

export const StyledInputTask = styled.div`
  box-sizing: border-box;

  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  margin: 10px 0 10px;
  padding: 10px 5px;
  width: 100%;
  min-height: 70px;
  background-color: #887;
  border-radius: 10px;
`;

export const StyledInput = styled.input`
  width: 90%;
  height: 45px;
  border: 2px solid black;
  border-radius: 10px;
  font-size: 24px;
`;

export const ButtonHolder = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  margin: 10px 0 5px;

  .btn {
    margin: 10px;
  }

  .btn:hover {
    transform: scale(0.8);
    transition: 0.4s;
    cursor: pointer;
  }
`;