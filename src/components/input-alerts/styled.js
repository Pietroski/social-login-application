import styled from 'styled-components';

export const StyledFormSpan = styled.span`
  color: darkred;
  font-weight: 500;
  align-self: flex-start;
  padding-left: 5%;
  margin-bottom: 10px;
`;