import React from 'react';
import renderer from 'react-test-renderer';

import FormSpan from '../';

describe('Snapshot Tests', () => {
  it('renders page/component', () => {
    const tree = renderer.create(<FormSpan />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
