import React from 'react';
import { StyledFormSpan } from './styled';

const FormSpan = ({ error }) => {
  return <StyledFormSpan>{error}</StyledFormSpan>;
};

export default FormSpan;
