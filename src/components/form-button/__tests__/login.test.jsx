import React from 'react';
import renderer from 'react-test-renderer';

import FormButton from '../index';

describe('Snapshot Tests', () => {
  it('renders page/component', () => {
    const tree = renderer.create(<FormButton />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
