import React from 'react';
import { StyledFormButton } from './styled';

const FormButton = (props) => {
  return <StyledFormButton {...props}>{props.title}</StyledFormButton>;
};

export default FormButton;
