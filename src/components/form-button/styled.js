import styled from 'styled-components';

export const StyledFormButton = styled.button`
  min-width: 85px;
  min-height: 35px;
  background-color: #1299d4;
  border: 3px solid black;
  border-radius: 5px;
  margin-top: 15px;
  color: #000;
  font-size: 20px;
`;