import React, { useState } from 'react';
import { StyledInput, StyledInputContainer, StyledInputIcon } from './styled';
import { BsEye, BsEyeSlash } from 'react-icons/bs';

const Input = (props) => {
  const [isHidden, setIsHidden] = useState(true);
  const [inputType, setInputType] = useState(props?.type);

  return (
    <StyledInputContainer>
      <StyledInput {...props} type={inputType} />
      {props.hideField ? (
        <StyledInputIcon>
          {!isHidden ? (
            <BsEye
              size={20}
              onClick={() => {
                setIsHidden(true);
                setInputType('password');
              }}
            />
          ) : (
            <BsEyeSlash
              size={20}
              onClick={() => {
                setIsHidden(false);
                setInputType('text');
              }}
            />
          )}
        </StyledInputIcon>
      ) : (
        <></>
      )}
    </StyledInputContainer>
  );
};

export default Input;
