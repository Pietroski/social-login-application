import styled from 'styled-components';

export const StyledInput = styled.input`
  width: 100%;
  height: 35px;
  margin: 5px 0 5px;
  border: 3px solid black;
  border-radius: 5px;
  padding-left: 0.25rem;
  font-size: 20px;
`;

export const StyledInputContainer = styled.div`
  width: 90%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const StyledInputIcon = styled.div(props => ({
  display: 'flex',
  flexFlow: 'column no wrap',
  justifyContent: 'center',
  alignItems: 'center',
  alignSelf: 'center',
  position: 'absolute',
  right: '10px',
  /* top: 'calc(35px / 2)', */
  zIndex: '2',
  color: 'black',
  '&:hover': {
    transform: 'scale(0.8)',
    transition: '0.2s',
    cursor: 'pointer',
  },
}));