import React from 'react';
import renderer from 'react-test-renderer';

import Input from '../';

describe('Snapshot Tests', () => {
  it('renders page/component', () => {
    const tree = renderer.create(<Input />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
