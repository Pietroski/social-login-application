import React from 'react';
import { useWindowSize } from '../../hooks';
import { StyledForm } from './styled';
import Input from '../input';
import FormButton from '../form-button';
import FormSpan from '../input-alerts';
import { LoginWithGoogleModule } from '../google';

const Form = (props) => {
  const [isDesktop] = useWindowSize();

  return (
    <StyledForm {...props} isDesktop={isDesktop}>
      <h1>{props.title}</h1>
      {props.children}
    </StyledForm>
  );
};

Form.Input = Input;
Form.Button = FormButton;
Form.Span = FormSpan;
Form.LWGM = LoginWithGoogleModule;

export default Form;
