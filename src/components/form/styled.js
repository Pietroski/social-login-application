import styled from 'styled-components';

export const StyledForm = styled.div(props => ({
    display: props?.display || "flex",
    flexFlow: props?.flexFlow || "column nowrap",
    justifyContent: props?.justifyContent || "center",
    alignItems: props?.alignItems || "center",
    width: props?.width || props?.isDesktop < 400 ? "90vw" : "350px",
    minHeight: props?.minHeight || "300px",
    backgroundColor: props?.backgroundColor || "darkgoldenrod",
    border: props?.border || "4px solid black",
    borderRadius: props?.borderRadius || "10px",
    h1: {
      color: 'black',
    }
}));