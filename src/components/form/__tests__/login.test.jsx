import React from 'react';
import renderer from 'react-test-renderer';

import Form from '../index';

describe('Snapshot Tests', () => {
  it('renders page/component', () => {
    const tree = renderer.create(<Form />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
