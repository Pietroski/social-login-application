import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { StyledMobileHeader } from './styled';
import { ImHome } from 'react-icons/im';
import { HiMenu } from 'react-icons/hi';
import { StyledGenericHeaderButton } from '../generic-styled';
import MobileLoggedHeader from './logged-mobile';
import MobileNonLoggedHeader from './non-logged-mobile';
import { useSelector } from 'react-redux';

const MobileHeader = () => {
  const isLogged = useSelector((state) => state.login.token);
  const history = useHistory();
  const [wasClicked, setWasClicked] = useState(false);

  return (
    <StyledMobileHeader>
      <StyledGenericHeaderButton onClick={() => history.push('/')}>
        <ImHome />
      </StyledGenericHeaderButton>
      <StyledGenericHeaderButton onClick={() => setWasClicked((prevState) => !prevState)}>
        <HiMenu size={32} />
      </StyledGenericHeaderButton>
      {wasClicked ? (
        isLogged ? (
          <MobileLoggedHeader isOn={setWasClicked} />
        ) : (
          <MobileNonLoggedHeader isOn={setWasClicked} />
        )
      ) : (
        <></>
      )}
    </StyledMobileHeader>
  );
};

export default MobileHeader;

// <MobileNonLoggedHeader isOn={setWasClicked}
