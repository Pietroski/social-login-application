import styled from 'styled-components';

export const StyledMobileLoggedHeader = styled.div`
  width: 150px;
  min-height: 100px;
  background-color: #555;
  z-index: 4;
  position: absolute;
  top: 70px;
  right: 5%;
  border-radius: 5px;
`;