import React from 'react';
import { StyledMobileLoggedHeader } from './styled';
import { useHistory, useLocation } from 'react-router-dom';
import {
  StyledBackgroundFloater,
  StyledHeaderLocation,
  StyledGenericHeaderButton,
} from '../styled';
import { useDispatch, useSelector } from 'react-redux';
import { logout_action } from '../../../../redux/actions';

const MobileLoggedHeader = ({ isOn }) => {
  const where = useLocation().pathname;
  const history = useHistory();
  const dispatch = useDispatch();
  const userId = useSelector((state) => state.login.user.id);

  return (
    <StyledBackgroundFloater onClick={() => isOn((prevState) => !prevState)}>
      <StyledMobileLoggedHeader onClick={() => isOn((prevState) => !prevState)}>
        <StyledHeaderLocation isThere={where === `/profile/${userId}`}>
          <StyledGenericHeaderButton
            onClick={() => {
              history.push(`/profile/${userId}`);
              isOn((prevState) => !prevState);
            }}>
            Profile
          </StyledGenericHeaderButton>
        </StyledHeaderLocation>
        <StyledHeaderLocation isThere={where === `/tasks/${userId}`}>
          <StyledGenericHeaderButton
            onClick={() => {
              history.push(`/tasks/${userId}`);
              isOn((prevState) => !prevState);
            }}>
            Tasks
          </StyledGenericHeaderButton>
        </StyledHeaderLocation>
        <StyledHeaderLocation>
          <StyledGenericHeaderButton
            onClick={() => {
              history.push('/');
              isOn((prevState) => !prevState);
              dispatch(logout_action());
            }}>
            Logout
          </StyledGenericHeaderButton>
        </StyledHeaderLocation>
      </StyledMobileLoggedHeader>
    </StyledBackgroundFloater>
  );
};

export default MobileLoggedHeader;
