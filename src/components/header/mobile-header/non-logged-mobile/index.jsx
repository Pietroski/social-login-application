import React from 'react';
import { StyledMobileNonLoggedHeader } from './styled';
import { useHistory, useLocation } from 'react-router-dom';
import {
  StyledBackgroundFloater,
  StyledHeaderLocation,
  StyledGenericHeaderButton,
} from '../styled';

const MobileNonLoggedHeader = ({ isOn }) => {
  const where = useLocation().pathname;
  const history = useHistory();
  return (
    <StyledBackgroundFloater onClick={() => isOn((prevState) => !prevState)}>
      <StyledMobileNonLoggedHeader onClick={() => isOn((prevState) => !prevState)}>
        <StyledHeaderLocation isThere={where === '/login'}>
          <StyledGenericHeaderButton
            onClick={() => {
              history.push('/login');
              isOn((prevState) => !prevState);
            }}>
            Login
          </StyledGenericHeaderButton>
        </StyledHeaderLocation>
        <StyledHeaderLocation className="bot" isThere={where === '/register'}>
          <StyledGenericHeaderButton
            onClick={() => {
              history.push('/register');
              isOn((prevState) => !prevState);
            }}>
            Register
          </StyledGenericHeaderButton>
        </StyledHeaderLocation>
      </StyledMobileNonLoggedHeader>
    </StyledBackgroundFloater>
  );
};

export default MobileNonLoggedHeader;
