import styled from 'styled-components';

export const StyledMobileHeader = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 80px;
  background-color: #444;
  padding-left: 5%;
  padding-right: 5%;
`;

export const StyledBackgroundFloater = styled.div`
  z-index: 2;
  position: absolute;
  margin: 0;
  padding: 0;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.25);
`;

export const StyledGenericHeaderButton = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  margin: 0 10px 0;
  font-size: 24px;
  width: auto;
  height: 50px;
  color: whitesmoke;
  cursor: pointer;

  &:hover {
    transform: scale(0.8);
    transition: linear 0.2s;
  }
`;

export const StyledHeaderLocation = styled.div`
  width: auto;
  height: 50px;
  cursor: pointer;
  border-bottom: ${(props) => props?.isThere && '4px solid #1299d4'};
`;