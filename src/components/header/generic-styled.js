import styled from 'styled-components';

export const StyledGenericHeaderButton = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  margin: 0 10px 0;
  font-size: 24px;
  width: auto;
  height: 80px;
  color: whitesmoke;
  cursor: pointer;

  &:hover {
    transform: scale(0.8);
    transition: linear 0.2s;
  }
`;

export const StyledHeaderLocation = styled.div`
  width: auto;
  height: 80px;
  cursor: pointer;
  border-bottom: ${(props) => props?.isThere && '4px solid #1299d4'};
`;