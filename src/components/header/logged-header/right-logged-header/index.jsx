import React from 'react';
import { StyledRightLoggedHeader } from './styled';
import { StyledGenericHeaderButton, StyledHeaderLocation } from '../../generic-styled';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logout_action } from '../../../../redux/actions';

const RightLoggedHeader = () => {
  const where = useLocation().pathname;
  const history = useHistory();
  const dispatch = useDispatch();
  const userId = useSelector((state) => state.login.user.id);

  return (
    <StyledRightLoggedHeader>
      <StyledHeaderLocation isThere={where === `/profile/${userId}`}>
        <StyledGenericHeaderButton onClick={() => history.push(`/profile/${userId}`)}>
          Profile
        </StyledGenericHeaderButton>
      </StyledHeaderLocation>
      <StyledHeaderLocation isThere={where === `/tasks/${userId}`}>
        <StyledGenericHeaderButton onClick={() => history.push(`/tasks/${userId}`)}>
          Tasks
        </StyledGenericHeaderButton>
      </StyledHeaderLocation>
      <StyledHeaderLocation>
        <StyledGenericHeaderButton
          onClick={() => {
            history.push('/');
            dispatch(logout_action());
          }}>
          Logout
        </StyledGenericHeaderButton>
      </StyledHeaderLocation>
    </StyledRightLoggedHeader>
  );
};

export default RightLoggedHeader;
