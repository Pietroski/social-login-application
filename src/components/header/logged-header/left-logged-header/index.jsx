import React from 'react';
import { StyledLeftLoggedHeader } from './styled';
import { StyledGenericHeaderButton, StyledHeaderLocation } from '../../generic-styled';
import { useHistory, useLocation } from 'react-router-dom';
import { ImHome } from 'react-icons/im';

const LeftLoggedHeader = () => {
  const where = useLocation().pathname;
  const history = useHistory();
  return (
    <StyledLeftLoggedHeader>
      <StyledHeaderLocation>
        <StyledGenericHeaderButton onClick={() => history.push('/')}>
          <ImHome />
        </StyledGenericHeaderButton>
      </StyledHeaderLocation>
    </StyledLeftLoggedHeader>
  );
};

export default LeftLoggedHeader;
