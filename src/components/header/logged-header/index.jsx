import React from 'react';
import { StyledLoggedHeader } from './styled';
import StyledRightLoggedHeader from './right-logged-header';
import StyledLeftLoggedHeader from './left-logged-header';

const LoggedHeader = () => {
  return (
    <StyledLoggedHeader>
      <StyledLeftLoggedHeader />
      <StyledRightLoggedHeader />
    </StyledLoggedHeader>
  );
};

export default LoggedHeader;
