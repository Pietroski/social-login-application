import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import NonLoggedHeader from './non-logged-header';
import LoggedHeader from './logged-header';
import MobileHeader from './mobile-header';
import { useWindowSize } from '../../hooks';

const nonHeaderUrls = ['/'];

const Header = () => {
  const [width] = useWindowSize();

  const isLogged = useSelector((state) => state.login.token);
  const isDesktop = width >= 400;
  const where = useLocation().pathname;

  if (nonHeaderUrls.some((e) => e === where)) return <></>;

  return isLogged ? (
    isDesktop ? (
      <LoggedHeader />
    ) : (
      <MobileHeader />
    )
  ) : isDesktop ? (
    <NonLoggedHeader />
  ) : (
    <MobileHeader />
  );
};

export default Header;
