import React from 'react';
import { StyledNonLoggedHeader } from './styled';
import StyledRightNonLoggedHeader from './right-non-logged-header';
import StyledLeftNonLoggedHeader from './left-non-logged-header';

const NonLoggedHeader = () => {
  return (
    <StyledNonLoggedHeader>
      <StyledLeftNonLoggedHeader></StyledLeftNonLoggedHeader>
      <StyledRightNonLoggedHeader></StyledRightNonLoggedHeader>
    </StyledNonLoggedHeader>
  );
};

export default NonLoggedHeader;
