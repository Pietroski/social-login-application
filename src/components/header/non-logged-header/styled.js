import styled from 'styled-components';

export const StyledNonLoggedHeader = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 80px;
  background-color: #444;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-around;
  align-items: center;
`;
