import React from 'react';
import { StyledLeftNonLoggedHeader } from './styled';
import { StyledGenericHeaderButton, StyledHeaderLocation } from '../../generic-styled';
import { useHistory, useLocation } from 'react-router-dom';
import { ImHome } from 'react-icons/im';

const LeftNonLoggedHeader = () => {
  const where = useLocation().pathname;
  const history = useHistory();
  return (
    <StyledLeftNonLoggedHeader>
      <StyledHeaderLocation>
        <StyledGenericHeaderButton onClick={() => history.push('/')}>
          <ImHome />
        </StyledGenericHeaderButton>
      </StyledHeaderLocation>
    </StyledLeftNonLoggedHeader>
  );
};

export default LeftNonLoggedHeader;
