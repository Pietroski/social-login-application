import styled from 'styled-components';

export const StyledRightNonLoggedHeader = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
`;
