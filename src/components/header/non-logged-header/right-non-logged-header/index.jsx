import React from 'react';
import { StyledRightNonLoggedHeader } from './styled';
import { StyledGenericHeaderButton, StyledHeaderLocation } from '../../generic-styled';
import { useHistory, useLocation } from 'react-router-dom';

const RightNonLoggedHeader = () => {
  const where = useLocation().pathname;
  const history = useHistory();
  return (
    <StyledRightNonLoggedHeader>
      <StyledHeaderLocation isThere={where === '/login'}>
        <StyledGenericHeaderButton onClick={() => history.push('/login')}>
          Login
        </StyledGenericHeaderButton>
      </StyledHeaderLocation>
      <StyledHeaderLocation isThere={where === '/register'}>
        <StyledGenericHeaderButton onClick={() => history.push('/register')}>
          Register
        </StyledGenericHeaderButton>
      </StyledHeaderLocation>
    </StyledRightNonLoggedHeader>
  );
};

export default RightNonLoggedHeader;
