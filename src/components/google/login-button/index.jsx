import React from 'react';
import { GoogleLogin, useGoogleLogin } from 'react-google-login';

const googleClientId = `366193723957-dhdha9fk63sd1looa8uf1rrgtvovqe22.apps.googleusercontent.com`;

const GoogleLoginButton = () => {
  const responseGoogle = (response) => {
    console.log(response);
    console.log(response.tokenId);
    // console.log(response.getBasicProfile());
    // console.log(response.getBasicProfile().getId());
    // console.log(response.getBasicProfile().getName());
    // console.log(response.getBasicProfile().getImageUrl());
    // console.log(response.getBasicProfile().getEmail());
    /*     console.log(response);
    console.log(response.profileObj);
    console.log(response.profileObj.email);
    console.log(response.profileObj.givenName);
    console.log(response.profileObj.familyName);
    console.log(response.profileObj.name);
    console.log(response.profileObj.imageUrl);
    console.log(response.profileObj.googleId);
    console.log(response.tokenId); */
  };

  return (
    <GoogleLogin
      clientId={googleClientId}
      buttonText="Login"
      onSuccess={responseGoogle}
      onFailure={responseGoogle}
      cookiePolicy={'single_host_origin'}></GoogleLogin>
  );
};

export default GoogleLoginButton;
