import React from 'react';
import { GoogleLogout, useGoogleLogout } from 'react-google-login';

const googleClientId = `366193723957-dhdha9fk63sd1looa8uf1rrgtvovqe22.apps.googleusercontent.com`;

const GoogleLogoutButton = () => {
  const responseGoogle = (response) => {
    console.log(response);
  };

  return (
    <GoogleLogout
      clientId={googleClientId}
      buttonText="Logout"
      onLogoutSuccess={responseGoogle}></GoogleLogout>
  );
};

export default GoogleLogoutButton;
