import React from 'react';
import { GoogleLoginButton } from '../';
import { StyledContainer } from './styled';

const LoginWithGoogleModule = () => {
  return (
    <StyledContainer>
      <div
        style={{
          border: '2px solid black',
          height: '1px',
          width: '90%',
          marginTop: '15px',
          boxSizing: 'border-box',
        }}
      />
      <p>or Login with Google!</p>
      <GoogleLoginButton />
    </StyledContainer>
  );
};

export default LoginWithGoogleModule;
