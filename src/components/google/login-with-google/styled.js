import styled from 'styled-components';

export const StyledContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  /* width: 350px; */
  width: 100%;
  margin-bottom: 20px;

  p {
    justify-self: center;
    align-self: center;
    text-align: center; 
  }
`;