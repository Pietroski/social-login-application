import React from 'react';
import { StyledTaskCard, StyledTask, ButtonHolder } from './styled';
import { ImCheckmark, ImCross } from 'react-icons/im';

const TaskCard = ({ task, cardId, onClick }) => {
  return (
    <StyledTaskCard>
      <StyledTask>{task}</StyledTask>
      <ButtonHolder>
        <ImCross color={'darkred'} size={20} className="btn" onClick={() => onClick(cardId)} />
        <ImCheckmark
          color={'darkgreen'}
          size={24}
          className="btn"
          onClick={() => onClick(cardId)}
        />
      </ButtonHolder>
    </StyledTaskCard>
  );
};

export default TaskCard;
