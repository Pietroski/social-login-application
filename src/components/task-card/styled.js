import styled from 'styled-components';

export const StyledTaskCard = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  margin: 10px 0 10px;
  padding: 10px 5px;
  width: 100%;
  min-height: 24pt;
  background-color: #887;
  border-radius: 10px;
  font-size: 18pt;
  text-align: center;
`;

export const StyledTask = styled.div``;

export const ButtonHolder = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;

  .btn {
    margin: 10px;
  }

  .btn:hover {
    transform: scale(0.8);
    transition: 0.4s;
    cursor: pointer;
  }
`;