import React from 'react';
import renderer from 'react-test-renderer';

import TaskCard from '../';

describe('Snapshot Tests', () => {
  it('renders page/component', () => {
    const tree = renderer.create(<TaskCard />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
