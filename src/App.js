import React from 'react';
import './App.css';
import styled from 'styled-components';
import Header from './components/header';
import Routes from './routes';

const App = () => {

  return (
    <Container>
      <Header />
      <Routes />
    </Container>
  );
};

export default App;

const Container = styled.div`
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: center;
`;
