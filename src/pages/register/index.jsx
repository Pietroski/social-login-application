import React, { useState, useEffect } from 'react';
import Form from '../../components/form/';
import { StyledRegister } from './styled';
import { useWindowSize } from '../../hooks';
import { register } from '../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const regexMail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

const Register = () => {
  const [width, height] = useWindowSize();
  const dispatch = useDispatch();
  const history = useHistory();
  // const hadError = useSelector((state) => state.login.hadError);

  const [firstNameInput, setFirstNameInput] = useState('');
  const [lastNameInput, setLastNameInput] = useState('');
  const [userInput, setUserInput] = useState('');
  const [mailInput, setMailInput] = useState('');
  const [passInput, setPassInput] = useState('');
  const [cPassInput, setCPassInput] = useState('');

  const handleFirstNameInput = ({ target: { value } }) => {
    setFirstNameInput(value);
  };

  const handleLastNameInput = ({ target: { value } }) => {
    setLastNameInput(value);
  };

  const handleUserInput = ({ target: { value } }) => {
    setUserInput(value);
  };

  const handleMailInput = ({ target: { value } }) => {
    setMailInput(value);
  };

  const handlePassInput = ({ target: { value } }) => {
    setPassInput(value);
  };

  const handleCPassInput = ({ target: { value } }) => {
    setCPassInput(value);
  };

  const [registerData, setRegisterData] = useState({
    firstName: firstNameInput,
    lastName: lastNameInput,
    username: userInput,
    email: mailInput,
    password: passInput,
    cPassword: cPassInput,
  });

  const [registerDataError, setRegisterDataError] = useState({
    firstNameError: '',
    lastNameError: '',
    usernameError: '',
    emailError: '',
    passwordError: '',
    cPasswordError: '',
  });

  const handleVerify = (callbackSetter) => {
    registerData.firstName.length < 4
      ? callbackSetter((prevState) => ({ ...prevState, firstNameError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, firstNameError: '' }));

    registerData.lastName.length < 4
      ? callbackSetter((prevState) => ({ ...prevState, lastNameError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, lastNameError: '' }));

    registerData.username.length < 3
      ? callbackSetter((prevState) => ({ ...prevState, usernameError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, usernameError: '' }));

    !regexMail.test(registerData.email)
      ? callbackSetter((prevState) => ({ ...prevState, emailError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, emailError: '' }));

    registerData.password.length < 4
      ? callbackSetter((prevState) => ({ ...prevState, passwordError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, passwordError: '' }));

    !(registerData.cPassword === registerData.password)
      ? callbackSetter((prevState) => ({ ...prevState, cPasswordError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, cPasswordError: '' }));

    return (
      registerData.firstName.length >= 4 &&
      registerData.lastName.length >= 4 &&
      registerData.username.length >= 4 &&
      registerData.password.length >= 4 &&
      regexMail.test(registerData.email) &&
      registerData.cPassword === registerData.password
    );
  };

  useEffect(() => {
    setRegisterData({
      firstName: firstNameInput,
      lastName: lastNameInput,
      username: userInput,
      email: mailInput,
      password: passInput,
      cPassword: cPassInput,
    });
  }, [firstNameInput, lastNameInput, userInput, mailInput, passInput, cPassInput]);

  const handleSubmit = (e) => {
    e.preventDefault();
    handleVerify(setRegisterDataError) && dispatch(register(registerData, history));
  };

  return (
    <StyledRegister {...[width, height]}>
      <Form title="Register" minHeight="500px">
        <Form.Input
          placeholder="First Name"
          type="text"
          value={firstNameInput}
          onChange={handleFirstNameInput}
        />
        <Form.Span error={registerDataError?.firstNameError} />
        <Form.Input
          placeholder="Last Name"
          type="text"
          value={lastNameInput}
          onChange={handleLastNameInput}
        />
        <Form.Span error={registerDataError?.lastNameError} />
        <Form.Input
          placeholder="Username"
          type="text"
          value={userInput}
          onChange={handleUserInput}
        />
        <Form.Span error={registerDataError?.usernameError} />
        <Form.Input placeholder="Email" type="text" value={mailInput} onChange={handleMailInput} />
        <Form.Span error={registerDataError?.emailError} />
        <Form.Input
          placeholder="Password"
          type="password"
          value={passInput}
          onChange={handlePassInput}
          hideField={true}
        />
        <Form.Span error={registerDataError?.passwordError} />
        <Form.Input
          placeholder="Confirm Password"
          type="password"
          value={cPassInput}
          onChange={handleCPassInput}
          hideField={true}
        />
        <Form.Span error={registerDataError?.cPasswordError} />
        <Form.Button title="Register" onClick={handleSubmit} />
        <Form.LWGM />
      </Form>
    </StyledRegister>
  );
};

export default Register;
