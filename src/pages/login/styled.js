import styled from 'styled-components';

export const StyledLogin = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  min-height: calc(100vh - 100px);
  margin: 10px 0 10px;
`;