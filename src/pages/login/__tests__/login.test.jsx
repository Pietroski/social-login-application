import React from 'react';
import renderer from 'react-test-renderer';

import Login from '../index';

describe('Snapshot Tests', () => {
  it('renders page/component', () => {
    const tree = renderer.create().toJSON();
    expect(tree).toMatchSnapshot();
  });
});
