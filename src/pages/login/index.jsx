import React, { useState, useEffect } from 'react';
import Form from '../../components/form/';
import { StyledLogin } from './styled';
import { useDispatch, useSelector } from 'react-redux';
import { login_action } from '../../redux/actions';
import { useHistory } from 'react-router-dom';

const regexMail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

const Login = () => {
  const [mailInput, setMailInput] = useState('');
  const [passInput, setPassInput] = useState('');

  const history = useHistory();
  const dispatch = useDispatch();
  // const hadError = useSelector((state) => state.login.hadError);

  const handleMailInput = ({ target: { value } }) => {
    setMailInput(value);
  };

  const handlePassInput = ({ target: { value } }) => {
    setPassInput(value);
  };

  const [loginData, setLoginData] = useState({ email: mailInput, password: passInput });
  const [loginDataError, setLoginDataError] = useState({ emailError: '', passwordError: '' });

  const handleVerify = (callbackSetter) => {
    !regexMail.test(loginData.email)
      ? callbackSetter((prevState) => ({ ...prevState, emailError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, emailError: '' }));

    loginData.password.length < 4
      ? callbackSetter((prevState) => ({ ...prevState, passwordError: 'invalid format!!' }))
      : callbackSetter((prevState) => ({ ...prevState, passwordError: '' }));

    return loginData.email.length >= 4 && loginData.password.length >= 4;
  };

  useEffect(() => {
    setLoginData({ email: mailInput, password: passInput });
  }, [mailInput, passInput]);

  const handleSubmit = (e) => {
    e.preventDefault();
    handleVerify(setLoginDataError) && dispatch(login_action(loginData, history));
  };

  return (
    <StyledLogin>
      <Form title="Login" minHeight="425px">
        <Form.Input placeholder="Email" type="text" value={mailInput} onChange={handleMailInput} />
        <Form.Span error={loginDataError?.emailError} />
        <Form.Input
          placeholder="Password"
          type="password"
          value={passInput}
          onChange={handlePassInput}
          hideField={true}
        />
        <Form.Span error={loginDataError?.passwordError} />
        <Form.Button title="Login" onClick={handleSubmit} />
        <Form.LWGM />
      </Form>
    </StyledLogin>
  );
};

export default Login;
