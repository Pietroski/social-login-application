import Login from './login';
import Register from './register';
import TaskPage from './tasks';

export { Login, Register, TaskPage};