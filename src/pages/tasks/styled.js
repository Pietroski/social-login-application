import styled from 'styled-components';

export const StyledTaskPage = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  min-height: calc(100vh - 100px);
  margin: 10px 0 10px;
  background-color: #282c34;
`;

export const StyledTaskHolder = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  justify-content: flex-start;
  align-items: center;
  margin: 10px 0 10px;
  padding: 10px 10px;
  width: ${props => props.width >= 790 ? "750px" : `95%`};
  min-height: 100px;
  background-color: #777;
  border-radius: 10px;

  .add-icon:hover {
    transform: scale(0.8);
    transition: 0.4s;
    cursor: pointer;
  }
`;