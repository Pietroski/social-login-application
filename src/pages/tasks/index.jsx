import React, { useState } from 'react';
import { StyledTaskPage, StyledTaskHolder } from './styled';
import { RiAddCircleLine } from 'react-icons/ri';
import { useWindowSize } from '../../hooks';
import InputTask from '../../components/input-task';
import { useSelector, useDispatch } from 'react-redux';
import TaskCard from '../../components/task-card';
import { post_task } from '../../redux/actions';

const TaskPage = () => {
  const [width] = useWindowSize();
  const [wasClicked, setWasClicked] = useState(false);

  const userTaskList = useSelector((state) => state.login.taskList);
  const token = useSelector((state) => state.login.token);
  const userId = useSelector((state) => state.login.user.id);
  const tasksToDisplay = userTaskList.filter((e) => e.userId === userId);
  const dispatch = useDispatch();

  const handleClick = (cardId) => {
    dispatch(post_task(userId, token, '', setWasClicked, 'delete', cardId));
  };

  return (
    <StyledTaskPage>
      <StyledTaskHolder width={width}>
        {wasClicked ? (
          <InputTask onHold={setWasClicked} />
        ) : (
          <RiAddCircleLine className="add-icon" size={72} onClick={() => setWasClicked(true)} />
        )}
        {tasksToDisplay.reverse().map((e) => (
          <TaskCard key={e.id} task={e.todo} cardId={e.id} onClick={handleClick} />
        ))}
      </StyledTaskHolder>
    </StyledTaskPage>
  );
};

export default TaskPage;
